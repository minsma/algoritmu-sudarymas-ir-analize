﻿using System;

namespace BucketSort
{
    class MyDataArray : DataArray
    {
        char[] data;

        public MyDataArray(int n, int seed)
        {
            data = new char[n];
            length = n;
            Random rand = new Random(seed);

            for(int i = 0; i < length; i++)
            {
                int num = rand.Next(0, 26);
                data[i] = (char)('a' + num);
            }
        }

        public override char this[int index]
        {
            get { return data[index]; }
        }

        public override void ChangeValue(int index, char value)
        {
            data[index] = value;
        }
    }
}
