﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BucketSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;

            Test(seed);
        }

        static void BucketSort(DataArray dataArray)
        {
            int minValue = dataArray[0];
            int maxValue = dataArray[0];

            for (int i = 1; i < dataArray.Length; i++)
            {
                if (dataArray[i] > maxValue)
                    maxValue = dataArray[i];
                if (dataArray[i] < minValue)
                    minValue = dataArray[i];
            }

            List<char>[] bucket = new List<char>[maxValue - minValue + 1];

            for (int i = 0; i < bucket.Length; i++)
            {
                bucket[i] = new List<char>();
            }

            for (int i = 0; i < dataArray.Length; i++)
            {
                bucket[dataArray[i] - minValue].Add(dataArray[i]);
            }

            int k = 0;
            for (int i = 0; i < bucket.Length; i++)
            {
                if (bucket[i].Count > 0)
                {
                    for (int j = 0; j < bucket[i].Count; j++)
                    {
                        dataArray.ChangeValue(k, bucket[i][j]);
                        k++;
                    }
                }
            }
        }

        static void PararelBucketSort(DataArray dataArray)
        {
            
        }

        static void Test(int seed)
        {
            int n = 12;

            MyDataArray myArray = new MyDataArray(n, seed);
            Console.WriteLine("\n Array \n");
            myArray.Print(n);
            BucketSort(myArray);
            Console.WriteLine("\n Sorted Array \n");
            myArray.Print(n);
        }
    }

    abstract class DataArray
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract char this[int index] { get; }
        public abstract void ChangeValue(int index, char value);
        public void Print(int n)
        {
            for(int i = 0; i < n; i++)
                Console.Write(this[i] + " ");
            Console.WriteLine();
        }
    }
}
