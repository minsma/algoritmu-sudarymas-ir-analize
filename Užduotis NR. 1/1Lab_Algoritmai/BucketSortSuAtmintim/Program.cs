﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

namespace BucketSortSuAtmintim
{
    class Program
    {
        static void Main(string[] args)
        {
            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;

            TestArrayAndList(seed);
            //TestSpeed(seed);
        }

        static void BucketSort(DataArray dataArray)
        {
            int minValue = dataArray[0];
            int maxValue = dataArray[0];

            for (int i = 1; i < dataArray.Length; i++)
            {
                if (dataArray[i] > maxValue)
                    maxValue = dataArray[i];
                if (dataArray[i] < minValue)
                    minValue = dataArray[i];
            }

            List<char>[] bucket = new List<char>[maxValue - minValue + 1];

            for (int i = 0; i < bucket.Length; i++)
            {
                bucket[i] = new List<char>();
            }

            for (int i = 0; i < dataArray.Length; i++)
            {
                bucket[dataArray[i] - minValue].Add(dataArray[i]);
            }

            int k = 0;
            for (int i = 0; i < bucket.Length; i++)
            {
                if (bucket[i].Count > 0)
                {
                    for (int j = 0; j < bucket[i].Count; j++)
                    {
                        dataArray.ChangeValue(k, bucket[i][j]);
                        k++;
                    }
                }
            }
        }

        static void BucketSort(DataList dataList)
        {
            int minValue = dataList.GetData(0);
            int maxValue = dataList.GetData(0);

            for (int i = 1; i < dataList.Length; i++)
            {
                if (dataList.GetData(i) > maxValue)
                    maxValue = dataList.GetData(i);
                if (dataList.GetData(i) < minValue)
                    minValue = dataList.GetData(i);
            }

            List<char>[] bucket = new List<char>[maxValue - minValue + 1];

            for (int i = 0; i < bucket.Length; i++)
            {
                bucket[i] = new List<char>();
            }

            for (int i = 0; i < dataList.Length; i++)
            {
                bucket[dataList.GetData(i) - minValue].Add(dataList.GetData(i));
            }

            int k = 0;
            for (int i = 0; i < bucket.Length; i++)
            {
                if (bucket[i].Count > 0)
                {
                    for (int j = 0; j < bucket[i].Count; j++)
                    {
                        dataList.SetData(k, bucket[i][j]);
                        k++;
                    }
                }
            }
        }

        static void TestArrayAndList(int seed)
        {
            int n = 12;
            string filename;
            filename = @"mydataarray.dat";
            MyFileArray myFileArray = new MyFileArray(filename, n, seed);

            using (myFileArray.fs = new FileStream(filename, FileMode.Open,
                                                    FileAccess.ReadWrite))
            {
                Console.WriteLine("\n FILE ARRAY \n");
                myFileArray.Print(n);
                BucketSort(myFileArray);
                Console.WriteLine("\n SORTED FILE ARRAY \n");
                myFileArray.Print(n);
            }

            filename = @"mydatalist.dat";
            MyFileList myFileList = new MyFileList(filename, n, seed);

            using (myFileList.fs = new FileStream(filename, FileMode.Open,
                                                    FileAccess.ReadWrite))
            {
                Console.WriteLine("\n FILE LIST \n");
                myFileList.Print(n);
                BucketSort(myFileList);
                Console.WriteLine("\n SORTED FILE LIST \n");
                myFileList.Print(n);
            }
        }

        static void TestSpeed(int seed)
        {
            int n = 100;
            int c = 7;

            Console.WriteLine("{0, 6} | {1, 16} | {2, 16}|", "KIEKIS", "Array", "List");

            for (int i = 0; i < c; i++)
            {
                Stopwatch timer = new Stopwatch();

                string filename;
                filename = @"mydataarray.dat";
                MyFileArray myFileArray = new MyFileArray(filename, n, seed);

                timer.Start();

                using (myFileArray.fs = new FileStream(filename, FileMode.Open,
                                                        FileAccess.ReadWrite))
                {
                    BucketSort(myFileArray);
                }

                timer.Stop();
                var arrayTime = timer.Elapsed.ToString();

                filename = @"mydatalist.dat";
                MyFileList myFileList = new MyFileList(filename, n, seed);

                timer.Reset();
                timer.Start();

                using (myFileList.fs = new FileStream(filename, FileMode.Open,
                                                        FileAccess.ReadWrite))
                {
                    BucketSort(myFileList);
                }

                timer.Stop();

                Console.WriteLine("{0, 6} | {1, -15} | {2, -15}|", n, arrayTime, timer.Elapsed.ToString());

                n *= 2;
            }
        }

    }

    abstract class DataArray
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract char this[int index] { get; }
        public abstract void ChangeValue(int index, char value);
        public void Print(int n)
        {
            for (int i = 0; i < n; i++)
                Console.Write(this[i] + " ");
            Console.WriteLine();
        }
    }

    abstract class DataList
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract char Head();
        public abstract char Next();
        public abstract void SetData(int index, char value);
        public abstract char GetData(int index);
        public void Print(int n)
        {
            Console.Write(Head() + " ");
            for (int i = 1; i < n; i++)
                Console.Write(Next() + " ");
            Console.WriteLine();
        }
    }
}
