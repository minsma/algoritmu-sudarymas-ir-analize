﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace DoubleHashSearch
{
    class Program
    {
        static Random random;

        static void Main(string[] args)
        {
            Test();
            //Test2();
        }

        static void Test()
        {
            List<string> nameList = new List<string>();
            DoubleHashTable<string, string> hashTable = new DoubleHashTable<string, string>();

            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;
            random = new Random(seed);

            int n = 15;
            int k = 15;

            for (int i = 0; i < n; i++)
            {
                string firstName = RandomName(k);
                string lastName = RandomName(k);
                nameList.Add(firstName);
                hashTable.put(firstName, lastName);
            }

            nameList.Add(RandomName(k));

            string[] list = hashTable.toArray();

            Console.WriteLine("Double Hash Table");

            foreach (string sublist in list)
            {
                Console.WriteLine(sublist);
            }

            Console.WriteLine("\n Search FILE Test \n");
            string fileName = @"mydatafile.dat";
            hashTable.WriteToFile(fileName, n);
            using (FileStream fs = new FileStream(fileName, FileMode.Open,
                                                  FileAccess.ReadWrite))
            {
                foreach (var s in nameList)
                {
                    Console.Write(hashTable.FileContains(fs, s, k).ToString() + " ");
                }
                Console.WriteLine("\n");
            }
        }

        static void Test2()
        {
            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;
            random = new Random(seed);

            int c = 7;
            int n = 100;

            Console.WriteLine("{0, 6} | {1, 16}|", "KIEKIS", "HashMapSuAtm");

            for (int i = 0; i < c; i++)
            {
                List<string> nameList = new List<string>();
                DoubleHashTable<string, string> hashTable = new DoubleHashTable<string, string>();

                for (int a = 0; a < n; a++)
                {
                    string firstName = RandomName(15);
                    string lastName = RandomName(15);
                    nameList.Add(firstName);
                    hashTable.put(firstName, lastName);
                }

                nameList.Add(RandomName(10));

                Stopwatch timer = new Stopwatch();
                timer.Start();

                string fileName = @"mydatafile.dat";

                using (FileStream fs = new FileStream(fileName, FileMode.Open,
                                                 FileAccess.ReadWrite))
                {
                    foreach (string name in nameList)
                    {
                        hashTable.FileContains(fs, name, 15);
                    }
                    timer.Stop();
                    Console.WriteLine("{0, 6} | {1, -15}|", n, timer.Elapsed.ToString());
                }


                n *= 2;
            }
        }

            static string RandomName(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 *
                                                                random.NextDouble() + 65)));
            builder.Append(ch);

            for (int i = 1; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 *
                                                               random.NextDouble() + 97)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}