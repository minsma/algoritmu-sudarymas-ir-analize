﻿using System;
using System.Text;

namespace DoubleHashSearch
{
    class DoubleHashTable<K, V>
    {
        public const int DEFAULT_INITIAL_CAPACITY = 16;
        public const float DEFAULT_LOAD_FACTOR = 0.75f;

        Entry<K, V>[] table;
        int size = 0;
        float loadFactor;
        int index = 0;

        public DoubleHashTable() : this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR)
        {
        }

        public DoubleHashTable(int initialCapacity, float loadFactor)
        {
            if (initialCapacity <= 0)
            {
                throw new ArgumentException("Illegal initial capacity: "
                        + initialCapacity);
            }

            if ((loadFactor <= 0.0) || (loadFactor > 1.0))
            {
                throw new ArgumentException("Illegal load factor: "
                        + loadFactor);
            }

            this.table = new Entry<K, V>[initialCapacity];
            this.loadFactor = loadFactor;
        }

        public bool isEmpty()
        {
            return size == 0;
        }

        public int tableSize()
        {
            return size;
        }

        public void clear()
        {
            Array.Fill(table, null);
            size = 0;
            index = 0;
        }

        public string[][] toArray()
        {
            string[][] result = new String[table.Length][];
            int count = 0;

            foreach (Entry<K, V> n in table)
            {
                string[] list = new string[1];

                int countLocal = 0;

                if (n != null)
                {
                    list[countLocal++] = n.ToString();
                }

                result[count] = list;
                count++;
            }

            return result;
        }

        public V put(K key, V value)
        {
            if (key == null || value == null)
            {
                throw new NullReferenceException("Key or value is null in "
                        + "put(Key key, Value value)");
            }

            int index = findPosition(key);

            if (index == -1)
            {
                rehash();
                put(key, value);
                return value;
            }

            if (table[index] == null)
            {
                table[index] = new Entry<K, V>(key, value);
                size++;

                if (size > table.Length * loadFactor)
                {
                    rehash();
                }
            }
            else
            {
                table[index].value = value;
            }

            return value;
        }

        public V get(K key)
        {
            if (key == null)
            {
                throw new NullReferenceException("Key is null in get(Key key)");
            }

            int index = findPosition(key);

            return (index != -1 && table[index] != null) ? table[index].value : default(V);
        }

        public bool contains(K key)
        {
            return get(key) != null;
        }

        public String toString()
        {
            StringBuilder result = new StringBuilder();

            foreach (Entry<K, V> entry in table)
            {
                if (entry != null)
                {
                    result.Append(entry.ToString()).Append('\n');
                }
            }

            return result.ToString();
        }

        private int findPosition(K key)
        {
            int index = hash(key);
            int index0 = index;
            int i = 0;

            for (int j = 0; j < table.Length; j++)
            {
                if (table[index] == null || table[index].key.Equals(key))
                {
                    return index;
                }

                i++;
                index = (index0 + index * hash2(key)) % table.Length;
            }

            return -1;
        }

        private int hash(K key)
        {
            int h = key.GetHashCode();

            return Math.Abs(h) % table.Length;
        }

        private int hash2(K key)
        {
            return 7 - (Math.Abs(key.GetHashCode()) % 7);
        }

        private void rehash()
        {
            DoubleHashTable<K, V> mapKTUOA
                    = new DoubleHashTable<K, V>(table.Length * 2, loadFactor);

            for (int i = 0; i < table.Length; i++)
            {
                if (table[i] != null)
                {
                    mapKTUOA.put(table[i].key, table[i].value);
                }
            }

            table = mapKTUOA.table;
        }

        class Entry<K, V>
        {
            public K key;
            public V value;

            public Entry()
            {
            }

            public Entry(K key, V value)
            {
                this.key = key;
                this.value = value;
            }

            public override String ToString()
            {
                return key + "=" + value;
            }
        }
    }

}
