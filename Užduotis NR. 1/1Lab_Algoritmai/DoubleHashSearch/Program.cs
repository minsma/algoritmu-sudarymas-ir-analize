﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace DoubleHashSearch
{
    class Program
    {
        static Random random;

        static void Main(string[] args)
        {
            var seed = (int)DateTime.Now.Ticks & 0x0000FFFF;
            random = new Random(seed);

            var nameList = new List<string>();
            var hashTable = new DoubleHashTable<string, string>();

            for (var i = 0; i < 50000; i++)
            {
                var firstName = RandomName(15);
                var lastName = RandomName(15);
                nameList.Add(firstName);
                hashTable.put(firstName, lastName);
            }

            nameList.Add(RandomName(10));

            var stopWatch = new Stopwatch();

            stopWatch.Start();
            int count = SequentialLoop(nameList, hashTable);
            stopWatch.Stop();

            Console.WriteLine("Time in milliseconds for sequential loop: {0,6:N0} ",
                                stopWatch.ElapsedMilliseconds);
            Console.WriteLine("Contains: {0,6:N0} Total: {1,6:N0}", count, nameList.Count);

            stopWatch.Reset();

            stopWatch.Start();
            count = ParallelTaskLoop(nameList, hashTable);
            stopWatch.Stop();

            Console.WriteLine("Time in milliseconds for parallel loop: {0,6:N0} ",
                                stopWatch.ElapsedMilliseconds);
            Console.WriteLine("Contains: {0,6:N0} Total: {1,6:N0}", count, nameList.Count);
        }

        private static int SequentialLoop(List<string> nameList, DoubleHashTable<string, string> hashTable)
        {
            int count = 0;

            for (int i = 0; i < nameList.Count; i++)
                if (hashTable.contains(nameList[i]))
                    count++;

            return count;
        }

        private static int ParallelTaskLoop(List<string> nameList, DoubleHashTable<string, string> hashTable)
        {
            int countCPU = 4;

            Task<int>[] tasks = new Task<int>[countCPU];

            for (var j = 0; j < countCPU; j++)
                tasks[j] = Task<int>.Factory.StartNew(
                (object p) =>
                {
                    int count = 0;

                    for (int i = (int)p; i < nameList.Count; i += countCPU)
                        if (hashTable.contains(nameList[i]))
                            count++;

                    return count;
                }, j);

            int total = 0;

            for (var i = 0; i < countCPU; i++)
                total += tasks[i].Result;

            return total;
        }

        static string RandomName(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 *
                                                                random.NextDouble() + 65)));
            builder.Append(ch);
            for (int i = 1; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 *
                                                               random.NextDouble() + 97)));
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }
}