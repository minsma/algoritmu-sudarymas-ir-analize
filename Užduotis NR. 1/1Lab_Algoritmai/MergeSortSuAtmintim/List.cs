﻿using System;
using System.IO;

namespace MergeSortSuAtmintim
{
    class MyFileList : DataList
    {
        int currentNode;
        int nextNode;

        public MyFileList(string filename, int n, int seed)
        {
            length = n;
            Random rand = new Random(seed);

            if (File.Exists(filename))
                File.Delete(filename);

            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(filename,
                                                              FileMode.Create)))
                {
                    writer.Write(4);
                    for (int j = 0; j < length; j++)
                    {
                        char symbol = (char)('a' + rand.Next(0, 25));
                        writer.Write(symbol);
                        writer.Write((j + 2) * 4 + j + 1);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public FileStream fs { get; set; }

        public override char Head()
        {
            Byte[] data = new Byte[6];

            fs.Seek(0, SeekOrigin.Begin);
            fs.Read(data, 0, 2);
            currentNode = BitConverter.ToInt32(data, 0);

            fs.Seek(currentNode, SeekOrigin.Begin);
            fs.Read(data, 0, 1);
            char result = BitConverter.ToChar(data, 0);

            currentNode = currentNode + 1;

            fs.Seek(currentNode, SeekOrigin.Begin);
            fs.Read(data, 0, 2);
            nextNode = BitConverter.ToInt32(data, 0);

            return result;
        }

        public override char Next()
        {
            Byte[] data = new Byte[6];

            fs.Seek(nextNode, SeekOrigin.Begin);
            fs.Read(data, 0, 1);
            char result = BitConverter.ToChar(data, 0);

            nextNode = nextNode + 1;
            currentNode = nextNode;

            fs.Seek(nextNode, SeekOrigin.Begin);
            fs.Read(data, 0, 2);
            nextNode = BitConverter.ToInt32(data, 0);

            return result;

        }

        public override char GetData(int index)
        {
            index -= 1;
            int position = (index + 2) * 4 + index + 1;

            Byte[] data = new Byte[2];
            fs.Seek(position, SeekOrigin.Begin);
            fs.Read(data, 0, 1);
            char result = BitConverter.ToChar(data, 0);

            return result;
        }

        public override void SetData(int index, char value)
        {
            index -= 1;
            int position = (index + 2) * 4 + index + 1;

            Byte[] data = new Byte[2];
            BitConverter.GetBytes(value).CopyTo(data, 0);
            fs.Seek(position, SeekOrigin.Begin);
            fs.Write(data, 0, 1);
        }
    }
}
