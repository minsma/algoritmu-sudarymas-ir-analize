﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace MergeSortSuAtmintim
{
    class MyFileArray : DataArray
    {
        public MyFileArray(string fileName, int n, int seed)
        {
            char[] data = new char[n];
            length = n;
            Random rand = new Random(seed);

            for (int i = 0; i < length; i++)
            {
                int num = rand.Next(0, 26);
                data[i] = (char)('a' + num);
            }

            if (File.Exists(fileName))
                File.Delete(fileName);

            try
            {
                using (BinaryWriter writer = new BinaryWriter(File.Open(fileName,
                                                              FileMode.Create)))
                {
                    for (int j = 0; j < length; j++)
                    {
                        writer.Write(data[j]);
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public FileStream fs { get; set; }

        public override char this[int index]
        {
            get
            {
                Byte[] data = new Byte[2];
                fs.Seek(index, SeekOrigin.Begin);
                fs.Read(data, 0, 1);

                char result = BitConverter.ToChar(data, 0);

                return result;
            }
        }

        public override void ChangeValue(int index, char value)
        {
            Byte[] data = new Byte[2];
            BitConverter.GetBytes(value).CopyTo(data, 0);
            fs.Seek(index, SeekOrigin.Begin);
            fs.Write(data, 0, 1);
        }
    }
}
