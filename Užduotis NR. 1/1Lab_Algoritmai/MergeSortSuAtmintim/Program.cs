﻿using System;
using System.Diagnostics;
using System.IO;

namespace MergeSortSuAtmintim
{
    class Program
    {
        const int MAX_VALUE = 10000;

        static void Main(string[] args)
        {
            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;

            TestArrayAndList(seed);
            //TestSpeed(seed);
        }

        static public void MainMerge(DataArray myArray, int left, int mid, int right)
        {
            char[] temp = new char[MAX_VALUE];
            int i, eol, num, pos;

            eol = (mid - 1);
            pos = left;
            num = (right - left + 1);

            while ((left <= eol) && (mid <= right))
            {
                if (myArray[left] <= myArray[mid])
                    temp[pos++] = myArray[left++];
                else
                    temp[pos++] = myArray[mid++];
            }

            while (left <= eol)
                temp[pos++] = myArray[left++];

            while (mid <= right)
                temp[pos++] = myArray[mid++];

            for (i = 0; i < num; i++)
            {
                myArray.ChangeValue(right, temp[right]);
                right--;
            }

        }
       
        static public void SortMerge(DataArray myArray, int left, int right)
        {
            int mid;

            if (right > left)
            {
                mid = (right + left) / 2;
                SortMerge(myArray, left, mid);
                SortMerge(myArray, (mid + 1), right);
                MainMerge(myArray, left, (mid + 1), right);
            }
        }

        static public void MainMerge(DataList myList, int left, int mid, int right)
        {
            char[] temp = new char[MAX_VALUE];
            int i, eol, num, pos;

            eol = (mid - 1);
            pos = left;
            num = (right - left + 1);

            while ((left <= eol) && (mid <= right))
            {
                if (myList.GetData(left) <= myList.GetData(mid))
                    temp[pos++] = myList.GetData(left++);
                else
                    temp[pos++] = myList.GetData(mid++);
            }

            while (left <= eol)
                temp[pos++] = myList.GetData(left++);

            while (mid <= right)
                temp[pos++] = myList.GetData(mid++);

            for (i = 0; i < num; i++)
            {
                myList.SetData(right, temp[right]);
                right--;
            }
        }

        static public void SortMerge(DataList myList, int left, int right)
        {
            int mid;

            if (right > left)
            {
                mid = (right + left) / 2;
                SortMerge(myList, left, mid);
                SortMerge(myList, (mid + 1), right);
                MainMerge(myList, left, (mid + 1), right);
            }
        }

        static void TestArrayAndList(int seed)
        {
            int n = 12;
            string filename;
            filename = @"mydataarray.dat";
            MyFileArray myFileArray = new MyFileArray(filename, n, seed);

            using (myFileArray.fs = new FileStream(filename, FileMode.Open,
                                                    FileAccess.ReadWrite))
            {
                Console.WriteLine("\n FILE ARRAY \n");
                myFileArray.Print(n);
                Console.WriteLine("\n SORTED FILE ARRAY \n");
                SortMerge(myFileArray, 0, n - 1);
                myFileArray.Print(n);
            }

            filename = @"mydatalist.dat";
            MyFileList myFileList = new MyFileList(filename, n, seed);

            using (myFileList.fs = new FileStream(filename, FileMode.Open,
                                                    FileAccess.ReadWrite))
            {
                Console.WriteLine("\n FILE LIST \n");
                myFileList.Print(n);
                Console.WriteLine("\n SORTED FILE ARRAY \n");
                SortMerge(myFileList, 0, n - 1);
                myFileList.Print(n);
            }
        }

        static void TestSpeed(int seed)
        {
            int n = 100;
            int c = 7;

            Console.WriteLine("{0, 6} | {1, 16} | {2, 16}|", "KIEKIS", "Array", "List");

            for (int i = 0; i < c; i++)
            {
                Stopwatch timer = new Stopwatch();

                string filename;
                filename = @"mydataarray.dat";
                MyFileArray myFileArray = new MyFileArray(filename, n, seed);

                timer.Start();

                using (myFileArray.fs = new FileStream(filename, FileMode.Open,
                                                        FileAccess.ReadWrite))
                {
                    SortMerge(myFileArray, 0, n - 1);
                }

                timer.Stop();
                var arrayTime = timer.Elapsed.ToString();

                filename = @"mydatalist.dat";
                MyFileList myFileList = new MyFileList(filename, n, seed);

                timer.Reset();
                timer.Start();

                using (myFileList.fs = new FileStream(filename, FileMode.Open,
                                                        FileAccess.ReadWrite))
                {
                    SortMerge(myFileList, 0, n - 1);
                }

                timer.Stop();

                Console.WriteLine("{0, 6} | {1, -15} | {2, -15}|", n, arrayTime, timer.Elapsed.ToString());

                n *= 2;
            }
        }

    }

    abstract class DataArray
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract char this[int index] { get; }
        public abstract void ChangeValue(int index, char value);
        public void Print(int n)
        {
            for (int i = 0; i < n; i++)
                Console.Write(this[i] + " ");
            Console.WriteLine();
        }
    }

    abstract class DataList
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract char Head();
        public abstract char Next();
        public abstract void SetData(int index, char value);
        public abstract char GetData(int index);
        public void Print(int n)
        {
            Console.Write(Head() + " ");
            for (int i = 1; i < n; i++)
                Console.Write(Next() + " ");
            Console.WriteLine();
        }
    }
}
