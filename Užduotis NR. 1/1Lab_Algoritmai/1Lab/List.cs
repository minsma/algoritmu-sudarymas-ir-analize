﻿using System;

namespace MergeSort
{
    class MyDataList : DataList
    {
        class MyLinkedListNode
        {
            public MyLinkedListNode nextNode { get; set; }
            public char data { get; set; }
            public MyLinkedListNode(char data)
            {
                this.data = data;
            }
        }

        MyLinkedListNode headNode;
        MyLinkedListNode currentNode;

        public MyDataList(int n, int seed)
        {
            length = n;
            Random rand = new Random(seed);
            int num = rand.Next(0, 26);
            headNode = new MyLinkedListNode((char)('a' + num));
            currentNode = headNode;

            for(int i = 1; i < length; i++)
            {
                num = rand.Next(0, 26);
                currentNode.nextNode = new MyLinkedListNode((char)('a' + num));
                currentNode = currentNode.nextNode;
            }

            currentNode.nextNode = null;
        }

        public override char Head()
        {
            currentNode = headNode;

            return currentNode.data;
        }

        public override char Next()
        {
            currentNode = currentNode.nextNode;

            return currentNode.data;
        }

        public override char GetData(int index)
        {
            MyLinkedListNode first = headNode;

            int i = 0;

            while(i < index)
            {
                first = first.nextNode;
                i++;
            }

            return first.data;
        }

        public override void SetData(int index, char value)
        {
            MyLinkedListNode first = headNode;

            int i = 0;

            while(i < index)
            {
                first = first.nextNode;
                i++;
            }

            first.data = value;
        }
    }
}
