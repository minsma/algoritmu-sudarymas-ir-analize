﻿using System;
using System.Diagnostics;

namespace MergeSort
{
    class Program
    {
        const int MAX_VALUE = 10000;

        static void Main(string[] args)
        {
            int seed = (int)DateTime.Now.Ticks & 0x0000FFFF;

            TestArrayAndList(seed);
            //TestSpeed(seed);
        }

        static public void MainMerge(MyDataArray myArray, int left, int mid, int right)
        {
            char[] temp = new char[MAX_VALUE];

            int eol = (mid - 1);
            int pos = left;
            int num = (right - left + 1);

            while ((left <= eol) && (mid <= right))
            {
                if (myArray[left] <= myArray[mid])
                    temp[pos++] = myArray[left++];
                else
                    temp[pos++] = myArray[mid++];
            }

            while (left <= eol)
                temp[pos++] = myArray[left++];

            while (mid <= right)
                temp[pos++] = myArray[mid++];

            for (int i = 0; i < num; i++)
            {
                myArray.changeValue(right, temp[right]);
                right--;
            }
        }

        static public void SortMerge(MyDataArray myArray, int left, int right)
        {
            if (right > left)
            {
                int mid = (right + left) / 2;
                SortMerge(myArray, left, mid);
                SortMerge(myArray, (mid + 1), right);
                MainMerge(myArray, left, (mid + 1), right);
            }
        }

        static public void MainMerge(MyDataList myList, int left, int mid, int right)
        {
            char[] temp = new char[MAX_VALUE];

            int eol = (mid - 1);
            int pos = left;
            int num = (right - left + 1);

            while ((left <= eol) && (mid <= right))
            {
                if (myList.GetData(left) <= myList.GetData(mid))
                    temp[pos++] = myList.GetData(left++);
                else
                    temp[pos++] = myList.GetData(mid++);
            }

            while (left <= eol)
                temp[pos++] = myList.GetData(left++);

            while (mid <= right)
                temp[pos++] = myList.GetData(mid++);

            for (int i = 0; i < num; i++)
            {
                myList.SetData(right, temp[right]);
                right--;
            }

        }

        static public void SortMerge(MyDataList myList, int left, int right)
        {
            if (right > left)
            {
                int mid = (right + left) / 2;
                SortMerge(myList, left, mid);
                SortMerge(myList, (mid + 1), right);
                MainMerge(myList, left, (mid + 1), right);
            }
        }

        static void TestArrayAndList(int seed)
        {
            int n = 12;

            MyDataArray myArray = new MyDataArray(n, seed);
            Console.WriteLine("\n Array \n");
            myArray.Print(n);
            SortMerge(myArray, 0, n - 1);
            Console.WriteLine("\n Sorted Array \n");
            myArray.Print(n);

            MyDataList myList = new MyDataList(n, seed);
            Console.WriteLine("\n LIST \n");
            myList.Print(n);
            SortMerge(myList, 0, n - 1);
            Console.WriteLine("\n Sorted LIST \n");
            myList.Print(n);
        }

        static void TestSpeed(int seed)
        {
            int n = 100;
            int c = 7;

            Console.WriteLine("{0, 6} | {1, 16} | {2, 16}|", "KIEKIS", "Array", "List");

            for (int i = 0; i < c; i++)
            {
                MyDataArray myArray = new MyDataArray(n, seed);
                MyDataList myList = new MyDataList(n, seed);

                Stopwatch timer = new Stopwatch();
                timer.Start();
                SortMerge(myArray, 0, n - 1);
                timer.Stop();
                var arrayTime = timer.Elapsed.ToString();

                timer.Reset();
                timer.Start();
                SortMerge(myList, 0, n - 1);
                timer.Stop();

                Console.WriteLine("{0, 6} | {1, -15} | {2, -15}|", n, arrayTime, timer.Elapsed.ToString());

                n *= 2;
            }
        }
    }

    abstract class DataArray
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract char this[int index] { get; }
        public abstract void changeValue(int index, char value);
        public void Print(int n)
        {
            for(int i = 0; i < n; i++)
                Console.Write(this[i] + " ");
            Console.WriteLine();
        }
    }

    abstract class DataList
    {
        protected int length;
        public int Length { get { return length; } }
        public abstract char Head();
        public abstract char Next();
        public abstract char GetData(int index);
        public abstract void SetData(int index, char value);
        public void Print(int n)
        {
            Console.Write(Head() + " ");
            for (int i = 1; i < n; i++)
                Console.Write(Next() + " ");
            Console.WriteLine();
        }
    }
}
