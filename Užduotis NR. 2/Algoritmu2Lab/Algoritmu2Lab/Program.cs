﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Algoritmu2Lab
{
    class Program
    {
        static int[] s;
        static int[] p;

        static void Main(string[] args)
        {
            s = new int[1000];
            p = new int[1000];

            for(int i = 0; i < 1000; i++)
            {
                s[i] = i + 1;
                p[i] = i + 2;
            }

            int k = 98;
            int r = 99;

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            int g = G(k, r);
            stopWatch.Stop();
            Console.WriteLine("Time in milliseconds for sequential G: {0,6:N0} ",
                stopWatch.ElapsedMilliseconds);
            Console.WriteLine("G( {0,4:N0} ) = {1,9:N0}", $"{k}, {r}", g);
            stopWatch.Reset();
            stopWatch.Start();
            int g2 = GParrarel(k, r);
            stopWatch.Stop();
            Console.WriteLine("Time in milliseconds for parallel G: {0,6:N0} ",
                stopWatch.ElapsedMilliseconds);
            Console.WriteLine("G( {0,4:N0} ) = {1,9:N0}", $"{k}, {r}", g2);
        }

        static int G(int k, int r)
        {
            if (r == 0 || k == 0)
                return 0;
            else if (s[k] > r)
                return G(k - 1, r);
            else
                return Math.Max(G(k - 1, r), p[k] + G(k - 1, r - s[k]));
        }

        static int GParrarel(int k, int r)
        {
            if (r == 0 || k == 0)
                return 0;
            else if (s[k] > r)
                return GParrarel(k - 1, r);
            else
            {
                var a = Task.Run(() => GParrarel(k - 1, r)).Result;
                var b = Task.Run(() => GParrarel(k - 1, r - s[k])).Result;

                return Math.Max(a, p[k] + b);
            }
        }
    }
}